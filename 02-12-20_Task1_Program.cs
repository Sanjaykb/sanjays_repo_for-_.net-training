using System;

namespace Project1ToPerform4TaskRM_02_12_20
{
    class Program
    {

        static void Processcases(char ch)
        {
            Task1To5noUniqueDisplay uobj = new Task1To5noUniqueDisplay();
            Task2ToTakeContinuousInput twoobj = new Task2ToTakeContinuousInput();
            Task3ToTakeListOf5Numbers thrdobj = new Task3ToTakeListOf5Numbers();
            Task4ToFindVowels fobj = new Task4ToFindVowels();
            switch (ch)
            {
                case '1':
                    uobj.ToDisplay5UniqNo();
                    break;
                case '2':
                    twoobj.ToTakeContinuousInput();
                    break;
                case '3':
                    thrdobj.TotakeListOf5CommaSeparatedNumber();
                    break;
                case '4':
                    fobj.FindVowels();
                    break;
               
                default:
                    Console.WriteLine("Invalid choice");
                    break;

            }
        }


        static void Main(string[] args)
        {
            //Task4ToFindVowels fobj = new Task4ToFindVowels();
            //Task1To5noUniqueDisplay uobj = new Task1To5noUniqueDisplay();
            //Task2ToTakeContinuousInput twoobj = new Task2ToTakeContinuousInput();
            //Task3ToTakeListOf5Numbers thrdobj = new Task3ToTakeListOf5Numbers();
           // int n = 1;
            while (true)
            {
                Console.WriteLine("Cases are as follows use them respectively to convert");
                Console.WriteLine("Enter '1' to try Task1- Display Five Unique no.");
                Console.WriteLine("Enter '2' to try Task2- Display list Without Duplicates ");
                Console.WriteLine("Enter '3' to try Task3-  Display List of 3 smallest no.");
                Console.WriteLine("Enter '4' to try Task4- Display Count of Vowels in a string");
                Console.WriteLine("Enter '0' to Exit ");
                Console.WriteLine("Please Enter your choice :");
                string choicest = Console.ReadLine();
                Char ch= choicest.ToCharArray()[0];
                if (ch == '0')
                {
                        Console.WriteLine("you choose to exit Bye !");
                    
                    break;
                }
                else
                {
                    Processcases(ch);
                }
               
            }
            Console.ReadKey();

            // uobj.ToDisplay5UniqNo();
            // twoobj.ToTakeContinuousInput();
            // thrdobj.TotakeListOf5CommaSeparatedNumber();
            //fobj.FindVowels();

        }
    }
}
