using System;

namespace CalcAreaPerimeCircle
{
    class Program
    {
          
        

        static void Main(string[] args)
        {
            Console.WriteLine("Program to calculate Area and Perimeter");
            CalculateCircle obj = new CalculateCircle();
            bool doContinue = true;
            do
            {
       
            obj.ReadInputs();
                Console.WriteLine("Do you want to continue ? Please press '1' to continue or '0' to abort and hit 'enter' key ");

                if (Console.ReadLine() == "1")
                {
                    doContinue = true;
                }
                else
                {
                    doContinue = false;
                }

                Console.ReadKey();

            } while (doContinue);
        }
    }
}
