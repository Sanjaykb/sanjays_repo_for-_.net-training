--Today's SQL Training Task

CREATE DATABASE Task_09_12_2020_Training;
--DROP DATABASE Task_09_12_2020_Training;
USE Task_09_12_2020_Training;
CREATE TABLE StudentInfo(
       [StudentID] [int] PRIMARY KEY NOT NULL,
	   [FirstName] [varchar](20) NULL,
       [LastName] [varchar](20) NULL,
       [Email] [varchar](20) NULL,
       [PhoneNumber] [varchar](10) NULL
 
) 
--DROP Table StudentInfo;
INSERT INTO [StudentInfo] ([StudentID],[FirstName] ,[LastName],[Email],[PhoneNumber] )
VALUES (100,'Vishal','Rajput','vrj@gmail.com','9878978901')
INSERT INTO [StudentInfo] ([StudentID],[FirstName] ,[LastName],[Email],[PhoneNumber] )
VALUES (101,'Suresh','Gputa','vrkj@gmail.com','1234567891')
INSERT INTO [StudentInfo] ([StudentID],[FirstName] ,[LastName],[Email],[PhoneNumber] )
VALUES (102,'Vishal','Kumar','vcj@gmail.com','9865421289')
INSERT INTO [StudentInfo] ([StudentID],[FirstName] ,[LastName],[Email],[PhoneNumber] )
VALUES (103,'Vikas','Yadav','vikse@gmail.com','9878978564')
INSERT INTO [StudentInfo] ([StudentID],[FirstName] ,[LastName],[Email],[PhoneNumber] )
VALUES (104,'Bhumi','Singh','shrut@gmail.com','9878978908')
INSERT INTO [StudentInfo] ([StudentID],[FirstName] ,[LastName],[Email],[PhoneNumber] )
VALUES (105,'Shruti','Mishra','shr@gmail.com','9878978978')
SELECT * FROM StudentInfo;

CREATE TABLE CourseCredentials(
       [StudentID] [int]  PRIMARY KEY NOT NULL,
	   [CourseName] [varchar](20) NOT NULL,
       [CourseDuration] [varchar](20)NOT NULL,
       [LoginID] [varchar](20)NOT NULL,
       [Password] [varchar](18) NOT NULL
 
) 

INSERT INTO [CourseCredentials]([StudentID],[CourseName],[CourseDuration],[LoginID],[Password])
VALUES (100,'B.Tech','4yrs','1001','vis100')
INSERT INTO [CourseCredentials]([StudentID],[CourseName],[CourseDuration],[LoginID],[Password])
VALUES (101,'B.Tech','4yrs','1011','sur101')
INSERT INTO [CourseCredentials]([StudentID],[CourseName],[CourseDuration],[LoginID],[Password])
VALUES (102,'M.Tech','2yrs','1021','vish102')
INSERT INTO [CourseCredentials]([StudentID],[CourseName],[CourseDuration],[LoginID],[Password])
VALUES (103,'M.Tech','2yrs','1031','vik103')
INSERT INTO [CourseCredentials]([StudentID],[CourseName],[CourseDuration],[LoginID],[Password])
VALUES (104,'B.E.','4yrs','1041','bhu104')
INSERT INTO [CourseCredentials]([StudentID],[CourseName],[CourseDuration],[LoginID],[Password])
VALUES (105,'B.E.','4yrs','1051','shru105')


SELECT * FROM CourseCredentials;

--InnerJoin
--SELECT * FROM StudentInfo INNER JOIN CourseCredentials ON StudentInfo.StudentID = CourseCredentials.StudentID
SELECT StudentInfo.StudentID,StudentInfo.FirstName,StudentInfo.LastName,StudentInfo.PhoneNumber,StudentInfo.Email FROM StudentInfo
Inner join CourseCredentials ON StudentInfo.StudentID = CourseCredentials.StudentID;
--CourseCredentials.CourseName,CourseCredentials.CourseDuration,CourseCredentials.LoginID,CourseCredentials.Password


--Left outer JOIN

SELECT FirstName,LastName
FROM StudentInfo
LEFT JOIN CourseCredentials
ON StudentInfo.StudentID = CourseCredentials.StudentID;

--Right Outer JOIN
SELECT FirstName,LastName
FROM StudentInfo
RIGHT JOIN CourseCredentials
ON StudentInfo.StudentID = CourseCredentials.StudentID;

--Full Outer Join
SELECT FirstName,LastName
FROM StudentInfo
RIGHT JOIN CourseCredentials
ON StudentInfo.StudentID = CourseCredentials.StudentID;

--Where Clause--
SELECT * FROM StudentInfo WHERE StudentID<=103
--Order By
SELECT * FROM StudentInfo order by StudentID 
SELECT * FROM StudentInfo  order by StudentID DESC
--Group By--
--SELECT StudentID FROM StudentInfo GROUP BY FirstName,LastName
--SELECT StudentID + Email FROM StudentInfo  GROUP BY StudentID,FirstName, LastName;
SELECT FirstName+LastName FROM StudentInfo  GROUP BY FirstName,LastName;
SELECT StudentID, COUNT(*) AS "Number of Student"
FROM StudentInfo
WHERE LastName = 'Gupta'
GROUP BY StudentID;

SELECT StudentID, COUNT(*) AS "Number of Student"
FROM CourseCredentials
WHERE CourseName = 'B.tech'
GROUP BY StudentID;

--Having --
SELECT FirstName FROM StudentInfo HAVING StudentID>103 order by StudentID DESC

--IN Operator
SELECT * FROM StudentInfo WHERE StudentID In (101,103,105)

--Like Operator
SELECT * FROM StudentInfo WHERE FirstName Like 'Vikas'
SELECT * FROM StudentInfo WHERE FirstName Like  'V%'

--Between--
SELECT * FROM StudentInfo WHERE StudentID BETWEEN 100 AND  103

--Math Function 

SELECT MIN(StudentID)
FROM StudentInfo
WHERE FirstName Like  'V%'  ;

SELECT MAX(StudentID)
FROM StudentInfo
WHERE FirstName Like  'V%'  ;

SELECT AVG(StudentID)
FROM StudentInfo
--WHERE FirstName Like  'V%'  ;

                                           --Inbuilt Functions

--Data type conversion 
---- 1.int to float--
declare @integerValue int
SET @integerValue  =1234
select @integerValue AS BeforeConversion
SELECT CONVERT(float, @integerValue) AS ConvertedFloatValue

----2. Float to Int Cnversion--

--Declaring a float variable
declare @floatValue AS float
--Assign a float value to variable
SET @floatValue = 1132.12345 
--SELECT @floatValue AS BeforeConversionFloat
--Convert float value to integer
SELECT @floatValue AS BeforeConversionFloat,  
CONVERT(int, @floatValue) AS ConvertedIntValue,
CONVERT(varchar, @FloatValue) AS ConvertedVarcharValue

----3. Float to Varchar Cnversion--
DECLARE @FloatValue1 AS Float
--Assign a float value to variable
SET @FloatValue1 = 7891.16545 
--Convert float value to varchar
SELECT CONVERT(varchar, @FloatValue1) AS ConvertedVarcharValue

----4. Decimal to Other Data type Cnversion--
DECLARE @DecimalValue2 AS decimal(10,3)
SET @DecimalValue2 =546.989

SELECT @DecimalValue2 AS DecimalValueBeforeConversion,
CONVERT(float,@DecimalValue2) AS ConvertedDecimalToFloatValue,
CONVERT(int,@DecimalValue2) AS ConvertedDecimalToIntValue;

----5. Bigint to Other Data type Cnversion--
DECLARE @BigintValue AS bigint
SET @BigintValue =546787667877876565
SELECT @BigintValue AS BigintValueBeforeConversion,
CONVERT(float,@BigintValue) AS ConvertedBigintValueToFloatValue,

CAST(@BigintValue as int) AS ConvertedBigintValueToIntValue,
CAST(@BigintValue as tinyint) AS ConvertedBigintValueToTinyIntValue
;

----5. Varchar to Other Data type Cnversion--

DECLARE @VarcharData as varchar(50)
SET @VarcharData = '1000.90'
SELECT @VarcharData AS BeforeConversionVarcharData,
CONVERT(float,@VarcharData) AS ConvertedToFloatValue;

--
DECLARE @VarcharData1 as varchar(50)
SET @VarcharData1 = '1000'
SELECT @VarcharData1 AS BeforeConversionVarcharData1,
CAST(@VarcharData1 as int) AS ConvertedVarcharDataToIntValue ;

--Unique Identifier--
DECLARE @UniqueData as Uniqueidentifier

SELECT @UniqueData


--Date different formats--
DECLARE @dataData1 as date
SET @dataData1 = GETDATE()

select @dataData1 AS DefaultDataFormat,
convert(varchar, @dataData1, 1),
convert(varchar, @dataData1, 2),
convert(varchar, @dataData1, 3),
convert(varchar, @dataData1, 4),
convert(varchar, @dataData1, 5),
convert(varchar, @dataData1, 6),
convert(varchar, @dataData1, 7),
convert(varchar, @dataData1, 10),
convert(varchar, @dataData1, 11),
convert(varchar, @dataData1, 12),
convert(varchar, @dataData1, 13),
convert(varchar, @dataData1, 23),
convert(varchar, @dataData1, 101),
convert(varchar, @dataData1, 102),
convert(varchar, @dataData1, 103),
convert(varchar, @dataData1, 104),
convert(varchar, @dataData1, 105),
convert(varchar, @dataData1, 106),
convert(varchar, @dataData1, 107),
convert(varchar, @dataData1, 110),
convert(varchar, @dataData1, 111),
convert(varchar, @dataData1, 112),
convert(varchar, @dataData1, 113),

--Date and Time Format--
DECLARE @dataData as datetime
SET @dataData = GETDATE()
select @dataData AS DefaultDataTimeFormat,
convert(varchar, @dataData, 0),
convert(varchar, @dataData, 9),
convert(varchar, @dataData, 13),
convert(varchar, @dataData, 20),
convert(varchar, @dataData, 21),
convert(varchar, @dataData, 22),
convert(varchar, @dataData, 25),
convert(varchar, @dataData, 100),
convert(varchar, @dataData, 109),
convert(varchar, @dataData, 113),
convert(varchar, @dataData, 120);

--Date addition--
DECLARE @dataData2 as date
SET @dataData2 = GETDATE()
SELECT @dataData2, 
Dateadd(MONTH,1,@dataData2),
Dateadd(DAY,1,@dataData2),
Dateadd(YEAR,1,@dataData2);

--
--Date Subtraction--
DECLARE @dataData3 as date
SET @dataData3 = GETDATE()
SELECT @dataData3, 
Dateadd(MONTH,-1,@dataData3),
Dateadd(DAY,-1,@dataData3),
Dateadd(YEAR,-1,@dataData3),

Datediff(MONTH,1,@dataData3),
Datediff(DAY,1,@dataData3),
Dateadd(YEAR,-1,@dataData3);

--Date Cast


--Date Time 2 Format
DECLARE @2ndDatetime2 datetime2(7)
SET @2ndDatetime2 = GETDATE()
SELECT @2ndDatetime2


--String data type functions: LTRIM, RTRIM, LEN ,Substring , Number AS String--
DECLARE @NumberInt as int
SET @NumberInt = 1000
DECLARE @StringDataSt VARCHAR(80)
SET @StringDataSt = '   SANJAY KUMAR   '
SELECT @StringDataSt AS OriginalString, 
TRIM(@StringDataSt) AS TrimmedString,
LTRIM(@StringDataSt) AS LeftTrimmedString,
RTRIM(@StringDataSt) AS RightTrimmedString,
LEN(@StringDataSt) AS LengthOfString,
TRIM('  SAR' FROM @StringDataSt) AS SpecificTrimmedString,
REPLACE(@StringDataSt, 'S', 'B')AS ReplacedString,

SUBSTRING(@StringDataSt, 3, 7) AS NewSubtring;


--Number int to unicode--
DECLARE @NumberInt1 as int
SET @NumberInt1 = 1000
SELECT NCHAR(@NumberInt1) AS NumberCodeToUnicode








