using System;
using System.Collections.Generic;
using System.Text;

namespace Project1ToPerform4TaskRM_02_12_20
{
    class Task3ToTakeListOf5Numbers
    {
        public void TotakeListOf5CommaSeparatedNumber()
        {
			string[] arr;

			while (true)
			{
				Console.WriteLine("Enter a list of comma separated numbers.");
				string inputst = Console.ReadLine();

				if (!String.IsNullOrEmpty(inputst))
				{
					arr = inputst.Split(',');
					if (arr.Length >5||arr.Length<5)
					Console.WriteLine("You have entered Invalid List. Please try again.");

					if (arr.Length == 5)
					{
						Console.Write("Great ! your Input is  : ");
						Console.WriteLine(inputst);
						break;
					}
				}
				else
					Console.WriteLine("You have entered Invalid List. Please try again.");
			}

			List<int> nList = new List<int>();

			foreach (var number in arr)
			{
				nList.Add(int.Parse(number));
			}
			nList.Sort();
			Console.WriteLine("Three Smallest Numbers in the entered List are as below:");
			
			for (int j = 0; j < 3; j++)
			{
				Console.WriteLine(nList[j]);
			}

			Console.WriteLine("<---------------Finished Task 3 --------------------> ");

		}
    }
}
