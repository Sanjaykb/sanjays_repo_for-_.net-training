using System;

namespace Task1ToChangedatatypesAsPerChoiceUsingIntparse
{
    class Program
    {
        
        static void Main(string[] args)
        {
            bool doContinue = true;
            do
            {

                Console.WriteLine("Enter a value to want to convert");
                String inputst = Console.ReadLine();
                Console.WriteLine("Cases are as follows use them respectively to convert");
                Console.WriteLine("Enter '1' to convert in to Integer ");
                Console.WriteLine("Enter '2' to convert in to float ");
                Console.WriteLine("Enter '3' to convert in to Double ");
                Console.WriteLine("Enter '4' to convert in to Long ");
                Console.WriteLine("Enter '5' to convert in to String ");
                Console.WriteLine("Enter your choice to convert : ");
                String choicest = Console.ReadLine();
                int usercase;
                var isParsed = int.TryParse(choicest, out int result);
                if (isParsed)
                {
                    usercase = int.Parse(choicest);
                }
                else
                {
                    usercase = result;
                    Console.WriteLine(" You have given Invalid input please input a number like from 1 to 5");
                    break;
                }
               
                Program.Processcases(usercase, inputst);
                Console.WriteLine("Do you want to continue ? Please press '1' to continue and '0' to abort");

                if (Console.ReadLine() == "1")
                {
                    doContinue = true;
                }
                else
                {
                    doContinue = false;
                }

                Console.ReadKey();

            } while (doContinue);

        }


        static void Processcases(int usercase, String inputst)
        {
            switch (usercase)
            {
                case 1:
                    Program.ToconvertInt(inputst);
                    break;
                case 2:
                    Program.Toconvertfloat(inputst);
                    break;
                case 3:
                    Program.Toconvertdouble(inputst);
                    break;
                case 4:
                    Program.Toconvertlong(inputst);
                    break;
                case 5:
                    Program.Toconvertchar(inputst);
                    break;
                default:
                    Console.WriteLine("Invalid choice");
                    break;

            }
        }

        static void ToconvertInt(String inputst)
        {
           
            int output = int.Parse(inputst);
            Console.WriteLine("Your output as per your choice Integer is :  " + output);
            

        }
        static void Toconvertfloat(String inputst)
        {

            
                float output = float.Parse(inputst);
                Console.WriteLine("Your outpot as per your choice converted to float is :  " + output);
        }
        static void Toconvertlong(String inputst)
        {

            
                long output = long.Parse(inputst);
                Console.WriteLine("Your outpot as per your choice converted to long is :  " + output);
            


        }
        static void Toconvertdouble(String inputst)
        {

          
            double output = double.Parse(inputst);
            Console.WriteLine("Your outpot as per your choice converted to double is :  " + output);

        }
        static void Toconvertchar(String inputst)
        {

                char[] array = inputst.ToCharArray();
                Console.WriteLine("Your outpot as per your choice converted to character  is :  ");
                for (int i = 0; i < array.Length; i++)
                {

                    char ch = array[i];

                    Console.WriteLine(ch);

                }

            }
            

        }



    }
    
