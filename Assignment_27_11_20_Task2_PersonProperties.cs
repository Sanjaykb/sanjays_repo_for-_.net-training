using System;
using System.Collections.Generic;

namespace Assignment_27_11_20_Task2_PersonProperties
{
    class Person
    {
        private string name;
        private int age;
        public string Name   // property
        {
            get
            { 
                return name; 
            }   // get method
            set {
                name = value;
            }  // set method
        }
        public int Age   // property
        {
            get
            {
                return age; 
            }  
           
            set 
            { 
                age = value; 
            }  
        }

       // List<Person> PersonList = new List<Person>() ;

        
        public void ls(List <Person> getlist)
        {
            Console.WriteLine("Persons having more than age 60 are as below");
            foreach (Person x in getlist)
            {
                if (x.age > 60)
                {
                    Console.WriteLine("Name ' "+x.name+" ' and his age is = " + x.age);
                }
            }
        }
       

    }

        class Program
    {
        static void Main(string[] args)
        {
            List<Person> PersonList = new List<Person>();
            PersonList.Add(new Person { Age = 65, Name = "Mukesh" });
            PersonList.Add(new Person { Age = 69, Name = "Suresh" });
            PersonList.Add(new Person { Age = 34, Name = "Mahesh" });
            PersonList.Add(new Person { Age = 22, Name = "Sanjay" });
            PersonList.Add(new Person { Age = 23, Name = "Vishal" });

            Person lsp = new Person();
            lsp.ls(PersonList);
            Console.ReadLine();
        }
    }
}
