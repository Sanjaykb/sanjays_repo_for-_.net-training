--Data type conversion Task of the day 08-12-2020-
---- 1.int to float--
declare @integerValue int
SET @integerValue  =1234
select @integerValue AS BeforeConversion
SELECT CONVERT(float, @integerValue) AS ConvertedFloatValue

----2. Float to Int Cnversion--

--Declaring a float variable
declare @floatValue AS float
--Assign a float value to variable
SET @floatValue = 1132.12345 
--SELECT @floatValue AS BeforeConversionFloat
--Convert float value to integer
SELECT @floatValue AS BeforeConversionFloat,  
CONVERT(int, @floatValue) AS ConvertedIntValue,
CONVERT(varchar, @FloatValue) AS ConvertedVarcharValue

----3. Float to Varchar Cnversion--
DECLARE @FloatValue1 AS Float
--Assign a float value to variable
SET @FloatValue1 = 7891.16545 
--Convert float value to varchar
SELECT CONVERT(varchar, @FloatValue1) AS ConvertedVarcharValue

----4. Decimal to Other Data type Cnversion--
DECLARE @DecimalValue2 AS decimal(10,3)
SET @DecimalValue2 =546.989

SELECT @DecimalValue2 AS DecimalValueBeforeConversion,
CONVERT(float,@DecimalValue2) AS ConvertedDecimalToFloatValue,
CONVERT(int,@DecimalValue2) AS ConvertedDecimalToIntValue;

----5. Bigint to Other Data type Cnversion--
DECLARE @BigintValue AS bigint
SET @BigintValue =546787667877876565
SELECT @BigintValue AS BigintValueBeforeConversion,
CONVERT(float,@BigintValue) AS ConvertedBigintValueToFloatValue,

CAST(@BigintValue as int) AS ConvertedBigintValueToIntValue,
CAST(@BigintValue as tinyint) AS ConvertedBigintValueToTinyIntValue
;

----5. Varchar to Other Data type Cnversion--

DECLARE @VarcharData as varchar(50)
SET @VarcharData = '1000.90'
SELECT @VarcharData AS BeforeConversionVarcharData,
CONVERT(float,@VarcharData) AS ConvertedToFloatValue;

--
DECLARE @VarcharData1 as varchar(50)
SET @VarcharData1 = '1000'
SELECT @VarcharData1 AS BeforeConversionVarcharData1,
CAST(@VarcharData1 as int) AS ConvertedVarcharDataToIntValue ;

--Unique Identifier--
DECLARE @UniqueData as Uniqueidentifier

SELECT @UniqueData


--Date different formats--
DECLARE @dataData1 as date
SET @dataData1 = GETDATE()

select @dataData1 AS DefaultDataFormat,
convert(varchar, @dataData1, 1),
convert(varchar, @dataData1, 2),
convert(varchar, @dataData1, 3),
convert(varchar, @dataData1, 4),
convert(varchar, @dataData1, 5),
convert(varchar, @dataData1, 6),
convert(varchar, @dataData1, 7),
convert(varchar, @dataData1, 10),
convert(varchar, @dataData1, 11),
convert(varchar, @dataData1, 12),
convert(varchar, @dataData1, 13),
convert(varchar, @dataData1, 23),
convert(varchar, @dataData1, 101),
convert(varchar, @dataData1, 102),
convert(varchar, @dataData1, 103),
convert(varchar, @dataData1, 104),
convert(varchar, @dataData1, 105),
convert(varchar, @dataData1, 106),
convert(varchar, @dataData1, 107),
convert(varchar, @dataData1, 110),
convert(varchar, @dataData1, 111),
convert(varchar, @dataData1, 112),
convert(varchar, @dataData1, 113),

--Date and Time Format--
DECLARE @dataData as datetime
SET @dataData = GETDATE()
select @dataData AS DefaultDataTimeFormat,
convert(varchar, @dataData, 0),
convert(varchar, @dataData, 9),
convert(varchar, @dataData, 13),
convert(varchar, @dataData, 20),
convert(varchar, @dataData, 21),
convert(varchar, @dataData, 22),
convert(varchar, @dataData, 25),
convert(varchar, @dataData, 100),
convert(varchar, @dataData, 109),
convert(varchar, @dataData, 113),
convert(varchar, @dataData, 120);

--Date addition--
DECLARE @dataData2 as date
SET @dataData2 = GETDATE()
SELECT @dataData2, 
Dateadd(MONTH,1,@dataData2),
Dateadd(DAY,1,@dataData2),
Dateadd(YEAR,1,@dataData2);

--
--Date Subtraction--
DECLARE @dataData3 as date
SET @dataData3 = GETDATE()
SELECT @dataData3, 
Dateadd(MONTH,-1,@dataData3),
Dateadd(DAY,-1,@dataData3),
Dateadd(YEAR,-1,@dataData3),

Datediff(MONTH,1,@dataData3),
Datediff(DAY,1,@dataData3),
Dateadd(YEAR,-1,@dataData3);

--Date Cast


--Date Time 2 Format
DECLARE @2ndDatetime2 datetime2(7)
SET @2ndDatetime2 = GETDATE()
SELECT @2ndDatetime2







