using System;

namespace Assignment_Task2
{
    class Program
    {
        static void Main(string[] args)
        {

            string[] monthNames = new string[] {"January", "February", "March", "April", "May",
  "June", "July", "August", "September", "October", "November", "December"};

            //Below Function also works on uncommenting I tried
            //string[] monthNames = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;

            foreach (string m in monthNames) 
            {
                if (m.StartsWith('J'))
                {
                    Console.WriteLine(m);
                }
                
            }
            Console.ReadKey();



        }
    }
}
