using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQpractice_18_12_20
{

    class Employee
    {
        public int EmpId { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
    }
    class Employees
    {
        public string Name { get; set; }
        public string Department { get; set; }
        public string Country { get; set; }
    }
    class Program
    {
        public static void LinqWithObject()
        {
            string str1 = "Welcome     to  LINQ Learning linq with object";
            var result = from s in str1.ToLowerInvariant().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                         select s;
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();
            string[] array = { "Vaishali", "Shalu", "Akshay", "Akki" };

            IEnumerable<string> res = from a in array
                                      where a.ToLowerInvariant().StartsWith("s")
                                      select a;

            foreach (string it in res)
            {
                Console.WriteLine(it);
            }
            Console.WriteLine();
            int[] numarray = { 1, 6, 9, 10, 50, 60, 100, 200, 300 };

            IEnumerable<int> resu = from a in numarray
                                    where a > 10 && a < 200
                                    select a;
            foreach (int ite in resu)
            {
                Console.WriteLine(ite);
            }

        }
        public static void ListAsEnu()
        {
            List<Employee> objEmp = new List<Employee>()
                {
                new Employee { EmpId=1,Name = "Akshay", Location="Chennai" },
                new Employee { EmpId=2,Name = "Vaishali", Location="Chennai" },
                new Employee { EmpId=3,Name = "Priyanka", Location="Guntur" },
                new Employee { EmpId=4,Name = "Preeti", Location ="Vizag"},
                };
            //LINQ query to sort or select the element from the collection of data  
            var result = from e in objEmp
                         where e.Location.Equals("Chennai")
                         select new
                         {
                             Name = e.Name,
                             Location = e.Location
                         };
           
            foreach (var item in result)
            {
                Console.WriteLine(item.Name + "\t | " + item.Location);
            }
        }
        //LINQ ToLookup() in Query Syntax
        public static void LookupLinQ()
        {
            List<Employees> objEmployee = new List<Employees>()
            {
                new Employees(){ Name="Ashish ", Department="Marketing", Country="India"},
                new Employees(){ Name="John", Department="IT", Country="Australia"},
                new Employees(){ Name="Kim", Department="Sales", Country="China"},
                new Employees(){ Name="Marcia", Department="HR", Country="USA"},
                new Employees(){ Name="John", Department="Operations", Country="Canada"}
            };
            var emp = (from employee in objEmployee select employee).ToLookup(x => x.Department);
            Console.WriteLine("Grouping Employees by Department");
            Console.WriteLine("---------------------------------");
            foreach (var KeyValurPair in emp)
            {
                Console.WriteLine(KeyValurPair.Key);
                // Lookup employees by Department  
                foreach (var item in emp[KeyValurPair.Key])
                {
                    Console.WriteLine("\t" + item.Name + "\t" + item.Department + "\t" + item.Country);
                }
            }

            // LINQ ToLookup() Operator in method syntax

            //var Emp = objEmployee.ToLookup(x => x.Department);
            //Console.WriteLine("Grouping Employees by Department");
            //Console.WriteLine("---------------------------------");
            //foreach (var KeyValurPair in Emp)
            //{
            //    Console.WriteLine(KeyValurPair.Key);
            //    // Lookup employees by Department  
            //    foreach (var item in Emp[KeyValurPair.Key])
            //    {
            //        Console.WriteLine("\t" + item.Name + "\t" + item.Department + "\t" + item.Country);
            //    }
            //}

        }
        static void Main(string[] args)
        {
           
            LinqWithObject();
            ListAsEnu();
            LookupLinQ();

            Console.ReadKey();
        }
    }
}
