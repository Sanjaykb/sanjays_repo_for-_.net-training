using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQpractice_17_12_20
{

    class Student
    {
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Location { get; set; }
    }

    class Department
    {
        public int DepId { get; set; }
        public string DepName { get; set; }
    }
    class Employee
    {
        public int EmpId { get; set; }
        public string Name { get; set; }
        public int DeptId { get; set; }
    }

    class Program
    {

        public static void ElementsOperator()
        {
            int[] objList = { 1, 2, 3, 4, 5 };
            //First() method is used to return the first element from the array 'objList'  
            int result = objList.First();
            Console.WriteLine("Element from the List: {0}", result);
            //the LINQ FIRST () operator in method syntax to get the first element from the collection.

            int res = (from l in objList select l).First();
            Console.WriteLine("Element from the List: {0}", res);
      // using the LINQ FirstOrDefault() operator in method syntax
            int[] ListObj = { 1, 2, 3, 4, 5 };
            int[] objVal = { };
            /*FirstOrDefault() method is used to return the first element from the 
                list and the list not contain any element will return the default value.*/
            int res1 = ListObj.FirstOrDefault();
            int val = objVal.FirstOrDefault();
            Console.WriteLine("Element from the List1: {0}", res1);
            Console.WriteLine("Element from the List2: {0}", val);

            // LINQ FirstOrDefault () operator in Query Syntax
            int res3 = (from l in ListObj select l).FirstOrDefault();

            int val1 = (from x in objVal

                       select x).FirstOrDefault();

            Console.WriteLine("Element from the List1: {0}", res3);

            Console.WriteLine("Element from the List2: {0}", val1);

            //Last element 
            int res4 = ListObj.Last();
            Console.WriteLine("Last Element from the List1: {0}", res4);

            //query syntax to fetch the last value from the list
            int res5 = (from l in ListObj select l).Last();
            Console.WriteLine("Last Element from the List: {0}", res5);

            // Last or default
            int res6= ListObj.FirstOrDefault();
            int val6 = objVal.FirstOrDefault();
            Console.WriteLine("Last Element from the List1: {0}", res6);
            Console.WriteLine("Last or default Element from the List2: {0}", val6);

            //ElementsAt
            int res7 = ListObj.ElementAt(1);
            int val7 = ListObj.ElementAt(3);
            Console.WriteLine("Element At particular index 1from the List1: {0}", res7);
            Console.WriteLine("Element At particular index 3 from the List2: {0}", val7);

            //LINQ ElementAtOrDefault() Method

            int res8 = ListObj.ElementAtOrDefault(1);
            int val8 = objVal.ElementAtOrDefault(3);
            Console.WriteLine("Element At particular index 1from the List1: {0}", res8);
            Console.WriteLine("Element At particular index 3 from the List2: {0}", val8);

            Console.WriteLine();

        }

        public static void LinqSingle()
        {
            List<Student> objStudent = new List<Student>()
            {
                new Student() { Name = "Shubham Rastogi", Gender = "Male",Location="Chennai" },
                new Student() { Name = "Rohini Tyagi", Gender = "Female", Location="Chennai" },
                new Student() { Name = "Praveen Alavala", Gender = "Male",Location="Bangalore" },
                new Student() { Name = "Sateesh Rastogi", Gender = "Male", Location ="Vizag"},
                new Student() { Name = "Madhav Sai", Gender = "Male", Location="Nagpur"}
            };
            //initialize the array objList  
            int[] objList = { 1 };
            //objStudent.Single() used to select the student  
            var user = objStudent.Single(s => s.Name == "Shubham Rastogi");
            string result = user.Name;
            int val = objList.Single();
            Console.WriteLine("Element from objStudent: {0}", result);
            Console.WriteLine("Element from objList: {0}", val);

            // SingleOr Default
            var user1 = objStudent.SingleOrDefault(i => i.Name == "Akshay Tyagi");
            string res9 = user1.Name;
            int val9 = objList.SingleOrDefault(j => j > 5);
            Console.WriteLine("Element from objStudent: {0}", res9);
            Console.WriteLine("Element from objList: {0}", val9);

            // LINQ SingleOrDefault() method will throw the InvalidOperationException 
            //error in case if the list/collection returns more than one element.

            Console.WriteLine();

        }
        public static void LinqDefault()
        {
            int[] b = { };
            int[] a = { 1, 2, 3, 4, 5 };

            Console.WriteLine("LinQ DefaultEmpty ()-");
            //with the help of DefaultfEmpty try to fetch the value from both of the list  
            var result = a.DefaultIfEmpty();
            var result1 = b.DefaultIfEmpty();
            Console.WriteLine("----List1 with Values----");
            //with the help of foreach loop we will print the value of 'result' variable   
            foreach (var val1 in result)
            {
                Console.WriteLine(val1);
            }
            Console.WriteLine("---List2 without Values---");
            //with the help of foreach loop we will print the value of 'result1' variable   
            foreach (var val2 in result1)
            {
                Console.WriteLine(val2);
            }

            Console.WriteLine();
        }
        public static void LINQGroupBy()
        {
            List<Student> objStudent2 = new List<Student>()
            {
            new Student() { Name = "Ak Tyagi", Gender = "Male",Location="Chennai" },
            new Student() { Name = "Rohini", Gender = "Female", Location="Chennai" },
            new Student() { Name = "Praveen", Gender = "Male",Location="Bangalore" },
            new Student() { Name = "Sateesh", Gender = "Male", Location ="Vizag"},
            new Student() { Name = "Madhav", Gender = "Male", Location="Nagpur"}
            };
            // here with the help of GrouBy we will fetch the student on the base of location  
            var student1 = objStudent2.GroupBy(x => x.Location);
            foreach (var sitem in student1)
            {
                // WriteLine() function here count the number of student  
                Console.WriteLine(sitem.Key, sitem.Count());
                Console.WriteLine();
                foreach (var stud in sitem)
                {
                    //Console.WriteLine(stud.Name + "\t" + stud.Location) show the information of the student on the base of the location  
                    Console.WriteLine(stud.Name + "\t" + stud.Location);
                }

                Console.WriteLine();

            }
            // LINQ GroupBy() in Query Syntax

            var student = from std in objStudent2
                          group std by std.Location;
            //foreach loop iterate over all the information of the student  
            foreach (var sitem in student)
            {
                Console.WriteLine(sitem.Key, sitem.Count());
                Console.WriteLine();
                foreach (var stud in sitem)
                {
                    Console.WriteLine(stud.Name + "\t" + stud.Location);
                }
                Console.WriteLine();
            }


            Console.WriteLine();
        }

        public static void JoinInLinq()
        {
            List<Department> Dept = new List<Department>
(){
    new Department{DepId=1,DepName="Software"},
    new Department{DepId=2,DepName="Finance"},
    new Department{DepId=3,DepName="Health"}
    };
            /* create an object of the class 'Employee' and create a 
            list of the Employee with the added record*/
            List<Employee> Emp = new List<Employee>
            ()
    {
    new Employee { EmpId=1,Name = "Akshay Tyagi", DeptId=1 },
    new Employee { EmpId=2,Name = "Vaishali Tyagi", DeptId=1 },
    new Employee { EmpId=3,Name = "Arpita Rai", DeptId=2 },
    new Employee { EmpId=4,Name = "Sateesh Alavala", DeptId =2},
    new Employee { EmpId=5,Name = "Madhav Sai"}
    };
            /*Linq query apply to fetch the information of the 
            employee name and department name 
            here we mapped the employee with the department and store the result in the variable 'result'*/
            var result = from d in Dept
                         join e in Emp
                         on d.DepId equals e.DeptId
                         select new
                         {
                             EmployeeName = e.Name,
                             DepartmentName = d.DepName
                         };
            //foreach loop traverse all the data in the result variable and store in item variable  
            foreach (var item in result)
            {
                /* Console.WriteLine(item.EmployeeName + "\t | " + item.DepartmentName) 
                 will print the name of the employee and name of the department*/
                Console.WriteLine(item.EmployeeName + "\t | " + item.DepartmentName);
            }
        }

        public static void LeftOuterJoinInLinq()
        {


            Console.WriteLine("Left Outer Join in Linq ");

            List<Department> objDept1 = new List<Department>()
        {
        new Department{DepId=1,DepName="Software"},
        new Department{DepId=2,DepName="Finance"},
        new Department{DepId=3,DepName="Health"}
        };
            foreach(var f in objDept1)
            {
                Console.WriteLine(f.DepId+"|"+f.DepName);
            }


            List<Employee> objEmp1 = new List<Employee>()
            {
            new Employee { EmpId=1,Name = "Akshay Tyagi", DeptId=1 },
            new Employee { EmpId=2,Name = "Vishi Tyagi", DeptId=1 },
            new Employee { EmpId=3,Name = "Arpita Rai", DeptId=2 },
            new Employee { EmpId=4,Name = "Mani", DeptId =2},
            new Employee { EmpId=5,Name = "Madhav Sai"}
            };
            /*use Linq Query to fetch the information by using the join clause  
            and check the department containing the employee or not */
            var result = from e in objEmp1
                         join d in objDept1
                         on e.DeptId equals d.DepId into empDept
                         from ed in empDept.DefaultIfEmpty()
                         select new
                         {
                             EmployeeName = e.Name,
                             DepartmentName = ed == null ? "No Department" : ed.DepName
                         };
            foreach (var item in result)
            {
                Console.WriteLine(item.EmployeeName + "\t | " + item.DepartmentName);
            }
        }
        public static void LinQSetOperation()
        {
            // Union
            string[] count1 = { "India", "USA", "UK", "Australia" };
            string[] count2 = { "India", "Canada", "UK", "China" };
            //count1.Union(count2) is used to find out the unique element from both the collection  
            var result = count1.Union(count2);
            //foreach loop is used to print the output conaining in the result  
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }

            var result1 = count1.Intersect(count2);
            foreach (var i in result1)
            {
                Console.WriteLine(i);
            }

            //Distinct
            string[] countries = { "UK", "India", "Australia", "uk", "india", "USA" };
              
            IEnumerable<string> result2 = countries.Distinct(StringComparer.OrdinalIgnoreCase); 
            foreach (var it in result2)
            {
                Console.WriteLine(it);
            }
            //except

            string[] a = { "Rohini", "Suresh", "Sateesh", "Praveen" };
            string[] b = { "Madhav", "Sushmitha", "Sateesh", "Praveen" };
            //Except method is used to return the value which does not exist in the second list   
            var result3 = a.Except(b);
            foreach (var ite in result3)
            {
                Console.WriteLine(ite);
            }

            string[] array1 = { "welcome", "to", "tutlane", "com" };
            string[] array2 = { "welcome", "TO", "Noida", "com" };
            string[] array3 = { "welcome", "to", "noida" };
            string[] array4 = { "WELCOME", "TO", "NOIDA" };
            //Sequence.Equal() method is used to check if both te sequences are equal or not  
            var res1 = array1.SequenceEqual(array2);
            var res2 = array1.SequenceEqual(array2, StringComparer.OrdinalIgnoreCase);
            var res3 = array1.SequenceEqual(array3);
            var res4 = array3.SequenceEqual(array4, StringComparer.OrdinalIgnoreCase);
            Console.WriteLine("Result1: {0}", res1);
            Console.WriteLine("Result2: {0}", res2);
            Console.WriteLine("Result3: {0}", res3);
            Console.WriteLine("Result4: {0}", res4);
 
        }



            static void Main(string[] args)
        {
            ElementsOperator();
           // LinqSingle();
            LinqDefault();
            LINQGroupBy();
            //JoinInLinq();
            LeftOuterJoinInLinq();
            LinQSetOperation();

            Console.ReadKey();
        }
    }
}
