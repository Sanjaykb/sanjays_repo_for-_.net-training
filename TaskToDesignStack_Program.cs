using System;

namespace TaskToDesignStack
{
    class Program
    {
        DesignStack stackOperationObject = new DesignStack();
        public void ProcessCases(char userChoice)
        {
            switch (userChoice)
            {
                case '1':
                    //Console.WriteLine("Please Enter the oject to push in stack");
                    //string inputObject = Console.ReadLine();
                    stackOperationObject.Push();
                    break;
                case '2':
                    stackOperationObject.Pop();
                    break;
                case '3':
                    stackOperationObject.Clear();
                    break;
                case '4':
                    stackOperationObject.ShowStackObjects();
                    break;

                default:
                    Console.WriteLine("I didn't get you, Wrong Choice Sorry !");
                    break;

            }
        }


        static void Main(string[] args)
        {
            Program st = new Program();
            while (true)
            {
                Console.WriteLine("Cases are as follows use them respectively to try in Stack");
                Console.WriteLine("Enter '1' to call Push() of stack which insert the object in stack");
                Console.WriteLine("Enter '2' to call Pop() of stack which delets top most object of stack");
                Console.WriteLine("Enter '3' to call Clear() which will remove all the object of stack");
                Console.WriteLine("Enter '4' to show all the objects present in the stack");
                Console.WriteLine("Enter '0' to Exit ");
                Console.Write("Please Enter your choice :");
                string choicest = Console.ReadLine();
                Char userChoice = choicest.ToCharArray()[0];
                if (userChoice == '0')
                {
                    Console.WriteLine("you choose to exit Bye !");

                    break;
                }
                else
                {
                   st. ProcessCases(userChoice);
                }

            }
            Console.ReadKey();


        }
      
    }
}
