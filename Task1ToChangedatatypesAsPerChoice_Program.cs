using System;

namespace ToChangeDataTypesAsPerUserChoiceUsingSwitch
{
    class Program
    {
        static void Main(string[] args)
        {

            bool doContinue = true;
            do
            {

                Console.WriteLine("Enter a value to want to convert");
                string inputst = Console.ReadLine();
                Console.WriteLine("Cases are as follows use them respectively to convert");
                Console.WriteLine("Enter '1' to convert in to Integer ");
                Console.WriteLine("Enter '2' to convert in to float ");
                Console.WriteLine("Enter '3' to convert in to Double ");
                Console.WriteLine("Enter '4' to convert in to Long ");
                Console.WriteLine("Enter '5' to convert in to String ");
                Console.WriteLine("Enter your choice to convert");
                string choicest = Console.ReadLine();
                int usercase = int.Parse(choicest);
                bool isParsed;
                // using TryParse() method to change data types.
                switch (usercase)
                {
                    case 1:
                        isParsed = int.TryParse(inputst, out int opi);
                        if (isParsed)
                        {
                            Console.WriteLine("'" + inputst + "' is converted in to Intiger and your respective output =  " + opi);
                        }
                        else
                        {
                            Console.WriteLine("'" + inputst + "' can not be converted to integer as you want ");
                        }

                        break;

                    case 2:
                        isParsed = float.TryParse(inputst, out float opf);
                        if (isParsed)
                        {
                            Console.WriteLine("'" + inputst + "' is converted in to float and your respective output =  " + opf);
                        }
                        else
                        {
                            Console.WriteLine("'" + inputst + "' can not be converted in to float as you want ");
                        }

                        break;
                    case 3:
                        isParsed = double.TryParse(inputst, out double opd);
                        if (isParsed)
                        {
                            Console.WriteLine("'" + inputst + "' is converted in to Double and your respective output =  " + opd);
                        }
                        else
                        {
                            Console.WriteLine("'" + inputst + "' can not be converted in to double as you want Sorry !");
                        }
                        break;
                    case 4:
                        isParsed = long.TryParse(inputst, out long opl);
                        if (isParsed)
                        {
                            Console.WriteLine("'" + inputst + "' is converted in to Long and your respective output =  " + opl);
                        }
                        else
                        {
                            Console.WriteLine("'" + inputst + "' can not be converted in to long as you want Sorry ! ");
                        }
                        break;
                    case 5:
                        isParsed = char.TryParse(inputst, out char opc);
                        if (isParsed)
                        {
                            Console.WriteLine("'" + inputst + "' is converted in to Char and your respective output =  " + opc);
                        }
                        else
                        {
                            Console.WriteLine("'" + inputst + "' can not be converted in to char as you want Sorrry ! ");
                        }
                        break;
                    default:
                        Console.WriteLine("'" + inputst + "' For this input your choice is invailid Sorry !");
                        break;

                }


                Console.WriteLine("Do you want to continue ? Please press '1' to continue or '0' to abort and hit 'enter' key ");

                if (Console.ReadLine() == "1")
                {
                    doContinue = true;
                }
                else
                {
                    doContinue = false;
                }

                Console.ReadKey();

            } while (doContinue);

        }
    }
    
}