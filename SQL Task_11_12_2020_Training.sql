
-- SQL Task_11_12_2020_Training

USE TestDB1;
--- Common Table Expressions--
CREATE TABLE  EmployeeInfo(

       [EmpID] [int] PRIMARY KEY NOT NULL,
	   [Name] [varchar](20) NULL,
       [Gender] [varchar](10) NULL,
       [Email] [varchar](20)UNIQUE  NULL,
       [Salary] [int] NULL,
	   [Department] [varchar](20) 
) 
INSERT INTO EmployeeInfo([EmpID],[Name],[Gender],[Email],[Salary],[Department])
Values (102,'Sanjay','Male','skb@gail.com',45000,'SDE')
INSERT INTO EmployeeInfo([EmpID],[Name],[Gender],[Email],[Salary],[Department])
Values (103,'Ajay','Male','sAkb@gail.com',25000,'IT')
INSERT INTO EmployeeInfo([EmpID],[Name],[Gender],[Email],[Salary],[Department])
Values (104,'Vikas','Male','Vkb@gail.com',45000,'SDE')
INSERT INTO EmployeeInfo([EmpID],[Name],[Gender],[Email],[Salary],[Department])
Values (105,'Jay','Male','jkb@gail.com',125000,'ADMIN')
INSERT INTO EmployeeInfo([EmpID],[Name],[Gender],[Email],[Salary],[Department])
Values (106,'Swati','Female','swkb@gail.com',15000,'IT')
INSERT INTO EmployeeInfo([EmpID],[Name],[Gender],[Email],[Salary],[Department])
Values (107,'MUKESH','Male','mkbb@gail.com',85000,'ADMIN')

SELECT * FROM EmployeeInfo;

With EmpTotalSalary(Depnm,sumsal)
as
(
SELECT Department,SUM(Salary)FROM EmployeeInfo Group by Department
)
SELECT * FROM EmpTotalSalary where sumsal>40000


---UDF--
    ---Crating Scalar Function
CREATE FUNCTION GetEmployee(@Rno INT)  
 RETURNS VARCHAR(50)  
 AS  
BEGIN  
    RETURN (SELECT Name FROM EmployeeInfo WHERE EmpID=@Rno)  
END 
--PRINT dbo. GetEmployee(102) 
SELECT dbo.GetEmployee(102) ;
--Returning as Table
CREATE FUNCTION ufnEmpTable (@emid int)
RETURNS TABLE AS
RETURN
 SELECT * FROM EmployeeInfo where EmpID=@emid;
 --Checking ufnEmpTable Function returning table
SELECT * FROM ufnEmpTable(104);
SELECT Name,Email,Gender FROM ufnEmpTable(104);
   
   ----Crating Inline Table Valued Function
CREATE FUNCTION GetAllEmployees(@v1 INT)  
RETURNS TABLE  
AS  
RETURN  
    SELECT *FROM EmployeeInfo WHERE EmpID>=@v1
--Trying inline function 
SELECT * FROM GetAllEmployees(105);





   --Stored Procedure
CREATE TABLE Product
(ProductID INT, ProductName VARCHAR(100) )
SELECT * FROM Product;

INSERT INTO Product VALUES (680,'HL Road Frame - Black, 58')
,(706,'HL Road Frame - Red, 58')
,(707,'Sport-100 Helmet, Red')

CREATE TABLE ProductDescription
(ProductID INT, ProductDescription VARCHAR(800) )

 
INSERT INTO ProductDescription VALUES (680,'Replacement mountain wheel for entry-level rider.')
,(706,'Sturdy alloy features a quick-release hub.')
,(707,'Aerodynamic rims for smooth riding.')
SELECT * FROM ProductDescription;

CREATE PROCEDURE GetProductDesc
AS
BEGIN

 
SELECT P.ProductID,P.ProductName,PD.ProductDescription  FROM 
Product P
INNER JOIN ProductDescription PD ON P.ProductID=PD.ProductID
 
END
--Drop Procedure GetProductDesc
EXEC GetProductDesc;

--Stored Procedure with Parameter
CREATE PROCEDURE GetProductDesc_withparameters
(@PID INT)
AS
BEGIN
SELECT P.ProductID,P.ProductName,PD.ProductDescription  FROM 
Product P
INNER JOIN ProductDescription PD ON P.ProductID=PD.ProductID
WHERE P.ProductID=@PID
END

EXEC GetProductDesc_withparameters 706;

--Views 

CREATE TABLE Employees
(EmployeeID    INT NOT NULL, 
 FirstName     NVARCHAR(50) NOT NULL, 
 MiddleName    NVARCHAR(50) NULL, 
 LastName      NVARCHAR(75) NOT NULL, 
 Title         NVARCHAR(100) NULL, 
 HireDate      DATETIME NOT NULL, 
 VacationHours SMALLINT NOT NULL, 
 Salary        DECIMAL(19, 4) NOT NULL
);
INSERT INTO Employees SELECT 1, 'Ken', NULL, 'Sánchez', 'Sales Representative', '1/1/2016', 2080, 45000;
INSERT INTO Employees SELECT 2, 'Janice', NULL, 'Galvin', 'Sales Representative', '12/11/2016', 2080, 45000;

CREATE TABLE Products
(ProductID INT NOT NULL, 
 Name      NVARCHAR(255) NOT NULL, 
 Price     DECIMAL(19, 4) NOT NULL
);
INSERT INTO Products SELECT 1, 'Long-Sleeve Logo Jersey, S', 12.99;
INSERT INTO Products SELECT 2, 'Long-Sleeve Logo Jersey, M', 14.99;
INSERT INTO Products SELECT 3, 'Long-Sleeve Logo Jersey, L', 16.99;
INSERT INTO Products SELECT 4, 'Long-Sleeve Logo Jersey, XL', 18.99;
    

CREATE TABLE Sales
(SalesID    UNIQUEIDENTIFIER NOT NULL, 
 ProductID  INT NOT NULL, 
 EmployeeID INT NOT NULL, 
 Quantity   SMALLINT NOT NULL, 
 SaleDate   DATETIME NOT NULL
);
INSERT INTO Sales SELECT NEWID(), 1, 1, 4, '04/15/2016';
INSERT INTO Sales SELECT NEWID(), 2, 1, 1, '02/01/2016';
INSERT INTO Sales SELECT NEWID(), 3, 1, 2, '03/12/2016';
INSERT INTO Sales SELECT NEWID(), 2, 2, 2, '03/18/2016';
INSERT INTO Sales SELECT NEWID(), 3, 2, 1, '04/16/2016';
INSERT INTO Sales SELECT NEWID(), 4, 2, 2, '04/23/2016';

SELECT * FROM Employees;
SELECT * FROM Products;
SELECT * FROM Sales;

CREATE VIEW vEmployeesWithSales
AS
     SELECT DISTINCT 
            Employees.*
     FROM Employees
          JOIN Sales ON Employees.EmployeeID = Sales.EmployeeID;

SELECT * FROM vEmployeesWithSales;

-------Ranking Function

CREATE TABLE ExamResult (StudentName VARCHAR(70), Subject     VARCHAR(20), Marks       INT);
INSERT INTO ExamResult VALUES('Lily','Maths', 65);
INSERT INTO ExamResult VALUES('Lily', 'Science', 80);
INSERT INTO ExamResult VALUES('Lily','english',70);
INSERT INTO ExamResult VALUES('Isabella','Maths',50);
INSERT INTO ExamResult VALUES('Isabella','Science',70); 
INSERT INTO ExamResult VALUES('Isabella','english',90);
INSERT INTO ExamResult VALUES('Olivia','Maths', 55);
INSERT INTO ExamResult VALUES('Olivia','Science', 60);
INSERT INTO ExamResult VALUES('Olivia','english', 89);

SELECT Studentname, Subject, Marks, ROW_NUMBER() OVER(ORDER BY Marks) RowNumber
FROM ExamResult;

SELECT Studentname, Subject, Marks,ROW_NUMBER() OVER(ORDER BY Marks desc) RowNumber
FROM ExamResult;

SELECT Studentname,Subject, Marks,  RANK() OVER(PARTITION BY Studentname ORDER BY Marks DESC) Rank
FROM ExamResult
ORDER BY Studentname, Rank;

SELECT Studentname, Subject, Marks, RANK() OVER(ORDER BY Marks DESC) Rank
FROM ExamResult
ORDER BY Rank;

SELECT Studentname, Subject,  Marks,DENSE_RANK() OVER(ORDER BY Marks DESC) Rank
FROM ExamResult
ORDER BY Rank;

SELECT Studentname, Subject, Marks, DENSE_RANK() OVER(PARTITION BY Subject ORDER BY Marks DESC) Rank
FROM ExamResult
ORDER BY Studentname,Rank;
    --updating
Update Examresult set Marks=70 where Studentname='Isabella' and Subject='Maths'

SELECT Studentname, Subject, Marks, RANK() OVER(PARTITION BY StudentName ORDER BY Marks ) Rank
FROM ExamResult
ORDER BY Studentname, Rank;

SELECT Studentname, Subject,  Marks,  DENSE_RANK() OVER(PARTITION BY StudentName ORDER BY Marks ) Rank
FROM ExamResult
ORDER BY Studentname, Rank;

SELECT *,NTILE(3) OVER( ORDER BY Marks DESC) Rank
FROM ExamResult 
ORDER BY rank;

SELECT *, 
       NTILE(2) OVER(PARTITION  BY subject ORDER BY Marks DESC) Rank
FROM ExamResult
ORDER BY subject, rank;

--Real Use of Ranking
WITH StudentRanks AS
(
  SELECT *, ROW_NUMBER() OVER( ORDER BY Marks) AS Ranks
  FROM ExamResult
)
 
SELECT StudentName , Marks 
FROM StudentRanks
WHERE Ranks >= 1 and Ranks <=3
ORDER BY Ranks