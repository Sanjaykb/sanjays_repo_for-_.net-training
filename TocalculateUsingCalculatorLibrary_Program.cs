using System;
using CalculatorLibrary;

namespace CalculatorConsoleAppUsingOwnDLL
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This is my first ever Calulator Console app Using My own Dll file ");

            ToCalculate mathsCalc = new ToCalculate();
            Console.WriteLine("Enter the two no. for operation");
           int n1=int.Parse(Console.ReadLine());
            int n2 = int.Parse(Console.ReadLine());


            float substract = mathsCalc.Substract(n1, n2);
            float multiply = mathsCalc.Multiply(n1, n2);
            float devide = mathsCalc.Devide(n1, n2);
            float addition = mathsCalc.Add(n1,n2);

            Console.WriteLine(n1+"-"+n2+" Result after Substraction = "+substract);
            Console.WriteLine(n1 + "x" + n2 + " Result after Multiplication = " + multiply);
            Console.WriteLine(n1 + "/" + n2 + " Result after Division  = " + devide);
            Console.WriteLine(n1 + "+" + n2 + " Result after Addition = " + addition);

            Console.ReadLine();

        }
    }
}
