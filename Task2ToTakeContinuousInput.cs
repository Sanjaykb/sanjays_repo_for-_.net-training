using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Project1ToPerform4TaskRM_02_12_20
{
    class Task2ToTakeContinuousInput
    {
        public void ToTakeContinuousInput()
        {
            String inputst;
            bool isIntN;
            List<int> nList = new List<int>();
            Console.WriteLine("Enter a number ");
            inputst = Console.ReadLine();
            isIntN = int.TryParse(inputst, out int opi);
            nList.Add(opi);
            while (string.IsNullOrEmpty(inputst) || isIntN != true|| inputst != "Quit")
            {
                if(inputst == "Quit" || inputst == "quit" || inputst == "QUIT")
                {
                    break;
                }

                Console.WriteLine("Enter a Number or Type 'Quit' ");
                inputst = Console.ReadLine();
                isIntN = int.TryParse(inputst, out opi);
               
                nList.Add(opi);

            }

            nList = nList.Distinct().ToList();

            Console.WriteLine("List of Unique Numbers after quiting");

            for (int j = 0; j < nList.Count; j++)
            {
                Console.WriteLine();
                    Console.Write(nList[j]+", ");

            }
            Console.WriteLine("<---------------Finished Task 2 --------------------> ");
        }
    }
}
