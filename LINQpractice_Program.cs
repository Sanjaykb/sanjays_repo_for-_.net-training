using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Linq;

namespace LINQpractice1
{
    public class Student
    {

        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public int Age { get; set; }


        public string Name { get; set; }
        public string Gender { get; set; }
        public List<string> Subjects { get; set; }


    }

    class Program
    {
        public static void abc()
        {
            //list to store the countries type of string  
            List<string> countries = new List<string>();

            countries.Add("India");

            countries.Add("US");

            countries.Add("Australia");

            countries.Add("Russia");
            //use lambda expression to show the list of the countries  
            IEnumerable<string> result = countries.Select(x => x);
            //foreach loop to display the countries  

            foreach (var item in result)
            {

                Console.WriteLine(item);

            }
            // Displaying directly from list
            foreach (var item1 in countries)
            {

                Console.WriteLine(item1);

            }
           // Console.ReadKey();
        }
        public static void abc1()
        {
            IList<Student> studentList = new List<Student>() {
        new Student() { StudentID = 1, StudentName = "John", Age = 13} ,
        new Student() { StudentID = 2, StudentName = "Moin",  Age = 21 } ,
        new Student() { StudentID = 3, StudentName = "Bill",  Age = 18 } ,
        new Student() { StudentID = 4, StudentName = "Ram" , Age = 20} ,
        new Student() { StudentID = 5, StudentName = "Ron" , Age = 15 }    };

            // LINQ Query Syntax to find out teenager students
            var teenAgerStudent = from s in studentList
                                  where s.Age > 12 && s.Age < 20
                                  select s;
            Console.WriteLine("Teen age Students:");

            foreach (Student std in teenAgerStudent)
            {
                Console.WriteLine(std.StudentName);
            }
        }

        public static void LinQAggregateFun()
        {
            int[] a = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            Console.WriteLine("The Minimum value in the given array is:");
            /*Min() function is applied on the array 'a' 
             to find the minimum number from the array*/
            int minimumNum = a.Min();
            int max = a.Max();
            int count = a.Count();
            int sum = a.Sum();
            Console.WriteLine("The minimum Number is = {0} ", minimumNum);
            // Console.WriteLine("Max is = ", a.Max());
            //Console.WriteLine("Count is = ", a.Count());
            //Console.WriteLine("Sum is = ", a.Sum());
            Console.WriteLine("Max is = ", max);
            Console.WriteLine("Count is = ", count);
            Console.WriteLine("Sum is = ", sum);
            Console.WriteLine("Max is = "+ max);
            Console.WriteLine("Count is = "+ count);
            Console.WriteLine("Sum is = "+sum);

            int[] Num = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            //Console.WriteLine("Product of the element:");
            //Now  calculate the total Sum of the numbers by applying the Aggregate function  
            //double Average = Num.Aggregate((c, b) => c * b);
            double Average = Num.Aggregate((c, b) => c + b);

           // Console.WriteLine("Total Sum of  is { 1, 2, 3, 4, 5, 6, 7, 8, 9 } ", Average);
            Console.WriteLine("Total Sum of  { 1, 2, 3, 4, 5, 6, 7, 8, 9 } is  =  "+ Average);

        }

        public static void OrderbyOperator()
        {

            List<Student> Objstudent = new List<Student>(){
        new Student() { Name = "Suresh Dasari", Gender = "Male", Subjects = new List<string> { "Mathematics", "Physics" } },
        new Student() { Name = "Rohini Alavala", Gender = "Female", Subjects = new List<string> { "Entomology", "Botany" } },
        new Student() { Name = "Praveen Kumar", Gender = "Male", Subjects = new List<string> { "Computers", "Operating System", "Java" } },
        new Student() { Name = "Sateesh Chandra", Gender = "Male", Subjects = new List<string> { "English", "Social Studies", "Chemistry" } },
        new Student() { Name = "Madhav Sai", Gender = "Male", Subjects = new List<string> { "Accounting", "Charted" } }
        };
            var studentname = Objstudent.OrderBy(x => x.Name);
            Console.WriteLine();
            Console.WriteLine("Order by ");

            foreach (var st in studentname)
            {
                Console.WriteLine(st.Name);
            }
            Console.WriteLine("Order by Desescending  ");
            var studentname1 = Objstudent.OrderByDescending(y => y.Name);
            foreach (var st1 in studentname1)
            {
                Console.WriteLine(st1.Name);
            }
        }
        public static void ThenbyOperator()
        {
            List<Student> studentList = new List<Student>() {
        new Student() { StudentID = 1, StudentName = "John", Age = 13} ,
        new Student() { StudentID = 2, StudentName = "Moin",  Age = 21 } ,
        new Student() { StudentID = 3, StudentName = "Bill",  Age = 18 } ,
        new Student() { StudentID = 4, StudentName = "Ram" , Age = 20} ,
        new Student() { StudentID = 5, StudentName = "Ron" , Age = 15 }    };

            var studentname2 = studentList.OrderBy(x => x.StudentName).ThenBy(x => x.Age);

            Console.WriteLine("ThenBY Operator in LINQ ");
            foreach (var st2 in studentname2)
            {
                Console.WriteLine("Name = {0} studentid = {1}", st2.StudentName, st2.Age);
            }
            //Console.WriteLine("ThenBY Operator in LINQ ");
            //foreach (var st2 in studentname2)
            //{
            //    Console.WriteLine("studentid = {0}", st2.Age);
            //}

        }

        public static void TakeOperator()
        {
            Console.WriteLine(" using LINQ Take() operator in query syntax");
            string[] countries = { "India", "USA", "Russia", "China", "Australia", "Argentina" };

            IEnumerable<string> result = (from x in countries select x).Take(3);

            foreach (string s in result)

            {
                Console.WriteLine(s);
            }
            Console.WriteLine(" using LINQ Take() operator in method syntax");
            IEnumerable<string> result1 = countries.Take(3);
            foreach (string s1 in result1)
            {
                Console.WriteLine(s1);
            }
        }
            

        public static void TypeCast()
        {
            Console.WriteLine("Type cast() arrayList objects to string in LINQ ");
            ArrayList obj = new ArrayList();
            obj.Add("USA");

            obj.Add("Australia");

            obj.Add("UK");

            obj.Add("India");
            IEnumerable<string> result = obj.Cast<string>();

            foreach (var item in result)

            {

                Console.WriteLine(item);

            }


        }

        //converting the list of "NumArray" to IEnumerable type by using the AsEnumerable method.

        public static void TypeConvert()
        {
            Console.WriteLine("the list of Number Array to IEnumerable type by using the As Enumerable method ");

            int[] NumArray = new int[] { 1, 2, 3, 4, 5 };

            var result = NumArray.AsEnumerable();

            foreach (var number in result)
            {
                Console.WriteLine(number);
            }

            Console.WriteLine("LINQ OfType() operator to get the only string type of elements from the list");
            ArrayList obj1 = new ArrayList();
            obj1.Add("Australia");
            obj1.Add("India");
            obj1.Add("UK");
            obj1.Add("USA");
            obj1.Add(1);

            IEnumerable<string> result1 = obj1.OfType<string>();

            foreach (var item in result1)
            {
                Console.WriteLine(item);
            }

        }


        static void Main(string[] args)
        {
            abc();
            abc1();
            LinQAggregateFun();
            OrderbyOperator();
            ThenbyOperator();
            TakeOperator();
            TypeCast();
            TypeConvert();

            // Console.WriteLine(teenAgerStudent);
            Console.ReadKey();

        }
    }
}
