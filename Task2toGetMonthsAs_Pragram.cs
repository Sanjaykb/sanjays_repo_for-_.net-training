using System;

namespace Task2toGetMonths
{
    class Program
    {
        static void UsingforloopIteration(string[] arr)
        {
            Console.WriteLine("Using Normal 'for loop' Iteration ");
            Console.WriteLine("Months Starting with letter 'J' are listed below ");
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i].StartsWith('J'))
                {
                    Console.WriteLine(arr[i]);
                }
               
            }
        }
        static void UsingwhileLoop(string[] arr)
        {
            int i = 0;
            Console.WriteLine("Using Normal 'While loop' Iteration ");
            Console.WriteLine("Months Ending with letter 'r' are listed below ");
            while (i<arr.Length)
            {
                if (arr[i].EndsWith('r'))
                {
                    Console.WriteLine(arr[i]);
                }
                i++;
            }
        }
        static void UsingforeachLoop(string[] monthNames)
        {
            Console.WriteLine("Using Normal 'foreach loop' Iteration ");
            
            Console.WriteLine("Months Starting with letter 'S' are listed below ");
            foreach (string m in monthNames)
            {
                if (m.StartsWith('S'))
                {
                    Console.WriteLine(m);
                }

            }       
         

        }
        static void UsingInbuiltFunforMonthsArray()
        {
            string[] monthNames1 = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
            Console.WriteLine("Using Inbuilt function 'System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;'");
            Console.WriteLine("Months Starting with letter 'M' are listed below ");
            foreach (string mn in monthNames1)
            {
                if (mn.StartsWith('M'))
                {
                    Console.WriteLine(mn);
                }

            }

        }

        static void Main(string[] args)
        {
            string[] monthNames = new string[] {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

            Program.UsingforloopIteration(monthNames);
            Console.WriteLine();
            Program.UsingwhileLoop(monthNames);
            Console.WriteLine();
            Program.UsingforeachLoop(monthNames);
            Console.WriteLine();
            Program.UsingInbuiltFunforMonthsArray();

            Console.ReadKey();
        }
    }
}
