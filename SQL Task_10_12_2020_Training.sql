-- SQL Task_10_12_2020_Training

USE TestDB1;
CREATE TABLE StudentInfo(

       --[Serial No.][int] IDENTITY(1,1),
	      [Serial No.][int] IDENTITY(5,1),
		  
       [StudentID] [int] PRIMARY KEY NOT NULL
	   Check ( StudentID >=100),
	   [FirstName] [varchar](20) NULL,
       [LastName] [varchar](20) NULL,
       [Email] [varchar](20)UNIQUE  NULL,
       [PhoneNumber] [varchar](10) NULL,
	   [CourseID] [int] Default 59
) 
--DROP Table StudentInfo;
INSERT INTO [StudentInfo] ([StudentID],[FirstName] ,[LastName],[Email],[PhoneNumber] , [CourseID])
VALUES (100,'Vishal','Rajput','vrj@gmail.com','9878978901',55)
INSERT INTO [StudentInfo] ([StudentID],[FirstName] ,[LastName],[Email],[PhoneNumber],[CourseID])
VALUES (101,'Suresh','Gputa','vrkj@gmail.com','1234567891',56)
INSERT INTO [StudentInfo] ([StudentID],[FirstName] ,[LastName],[Email],[PhoneNumber], [CourseID])
VALUES (102,'Vishal','Kumar','vcj@gmail.com','9865421289',55)
INSERT INTO [StudentInfo] ([StudentID],[FirstName] ,[LastName],[Email],[PhoneNumber],[CourseID])
VALUES (103,'Vikas','Yadav','vikse@gmail.com','9878978564',57)
INSERT INTO [StudentInfo] ([StudentID],[FirstName] ,[LastName],[Email],[PhoneNumber],[CourseID])
VALUES (104,'Bhumi','Singh','shrut@gmail.com','9878978908',55)
INSERT INTO [StudentInfo] ([StudentID],[FirstName] ,[LastName],[Email],[PhoneNumber],[CourseID])
VALUES (105,'Shruti','Mishra','shr@gmail.com','9878978978',57)
INSERT INTO [StudentInfo] ([StudentID],[FirstName] ,[LastName],[Email],[PhoneNumber])
VALUES (06,'Sheru','Patel','sher@gmail.com','9878955978')
INSERT INTO [StudentInfo] ([StudentID],[FirstName] ,[LastName],[Email],[PhoneNumber])
VALUES (107,'Sheru','Patel','sher@gmail.com','9878955978')
--INSERT INTO [StudentInfo] ([StudentID],[FirstName] ,[LastName],[Email],[PhoneNumber])
--VALUES (1009,null,null,null,null)

--TRUNCATE TABLE StudentInfo;

SELECT * FROM StudentInfo;

CREATE TABLE CourseCredentials(
	   [CourseID] [int] PRIMARY KEY NOT NULL,
       --[StudentID] [int]  Foreign KEY References StudentInfo,
	   [CourseName] [varchar](20) UNIQUE NOT NULL,
       [CourseDuration] [varchar](20)NOT NULL,
       [LoginID] [varchar](20)NOT NULL,
       [Password] [varchar](18) NOT NULL
 
) 
--[StudentID],
INSERT INTO [CourseCredentials]([CourseID],[CourseName],[CourseDuration],[LoginID],[Password])
VALUES (55,'B.Tech','4yrs','1001','bis100')
INSERT INTO [CourseCredentials]([CourseID],[CourseName],[CourseDuration],[LoginID],[Password])
VALUES (56,'M.Tech','2yrs','1002','mis100')
INSERT INTO [CourseCredentials]([CourseID],[CourseName],[CourseDuration],[LoginID],[Password])
VALUES (57,'B.E.','4yrs','1004','beis100')
INSERT INTO [CourseCredentials]([CourseID],[CourseName],[CourseDuration],[LoginID],[Password])
VALUES (58,'Short Term Course','2 Months','5554','short100')
INSERT INTO [CourseCredentials]([CourseID],[CourseName],[CourseDuration],[LoginID],[Password])
VALUES (59,'Other Course','','','any100')

--INSERT INTO [CourseCredentials]([CourseID],[CourseName],[CourseDuration],[LoginID],[Password])
--VALUES (8009,0,'2 Months','null',null)
--Truncate Table CourseCredentials;
--DROP TABLE CourseCredentials;
SELECT * FROM CourseCredentials;
--Inner Join--
--It returns only matching rows from the joined table
SELECT StudentID,FirstName,LastName,Email,PhoneNumber,CourseName,CourseDuration,LoginID,Password
FROM StudentInfo
INNER JOIN CourseCredentials
ON StudentInfo.CourseID = CourseCredentials.CourseID;

--SELECT StudentID,FirstName,LastName,Email From StudentInfo And 

--Left Outer Join or Left Join
--It wil return matching as well as non matching rows basically left table rows comletely and matching from both table.
SELECT StudentID,FirstName,LastName,Email,PhoneNumber,CourseName,CourseDuration,LoginID,Password
FROM StudentInfo
LEFT Outer JOIN CourseCredentials
ON StudentInfo.CourseID = CourseCredentials.CourseID

--Right Outer Join Or Right Join--
--It Returns the Right table completely and matching records of both table as well
SELECT StudentID,FirstName,LastName,Email,PhoneNumber,CourseName,CourseDuration,LoginID,Password
FROM StudentInfo
RIGHT Outer JOIN CourseCredentials
ON StudentInfo.CourseID = CourseCredentials.CourseID;

--Full Outer Join
-- It Returns all the records matching and Non Matching from both the tables
SELECT StudentID,FirstName,LastName,Email,PhoneNumber,CourseName,CourseDuration,LoginID,Password
FROM StudentInfo
FULL Outer JOIN CourseCredentials
ON StudentInfo.CourseID = CourseCredentials.CourseID;

--Cross Join--
--It Don't Requires the ON Clause and it returns the Cartesian product lik1 No.of rows in FirstTable*No. of rows in SecondTable
SELECT StudentID,FirstName,LastName,Email,PhoneNumber,CourseName,CourseDuration,LoginID,Password
FROM StudentInfo
CROSS JOIN CourseCredentials
--ON StudentInfo.CourseID = CourseCredentials.CourseID;

--
--SELECT StudentID,FirstName From StudentInfo GROUP BY StudentID

SELECT StudentID,FirstName,LastName From StudentInfo Group by FirstName,LastName having StudentID>102

SELECT StudentID,FirstName,LastName From StudentInfo Group by StudentID,FirstName,LastName having StudentID>102

CREATE TABLE  EmployeeInfo(

       [EmpID] [int] PRIMARY KEY NOT NULL,
	   [Name] [varchar](20) NULL,
       [Gender] [varchar](10) NULL,
       [Email] [varchar](20)UNIQUE  NULL,
       [Salary] [int] NULL,
	   [Department] [varchar](20) 
) 
INSERT INTO EmployeeInfo([EmpID],[Name],[Gender],[Email],[Salary],[Department])
Values (102,'Sanjay','Male','skb@gail.com',45000,'SDE')
INSERT INTO EmployeeInfo([EmpID],[Name],[Gender],[Email],[Salary],[Department])
Values (103,'Ajay','Male','sAkb@gail.com',25000,'IT')
INSERT INTO EmployeeInfo([EmpID],[Name],[Gender],[Email],[Salary],[Department])
Values (104,'Vikas','Male','Vkb@gail.com',45000,'SDE')
INSERT INTO EmployeeInfo([EmpID],[Name],[Gender],[Email],[Salary],[Department])
Values (105,'Jay','Male','jkb@gail.com',125000,'ADMIN')
INSERT INTO EmployeeInfo([EmpID],[Name],[Gender],[Email],[Salary],[Department])
Values (106,'Swati','Female','swkb@gail.com',15000,'IT')
INSERT INTO EmployeeInfo([EmpID],[Name],[Gender],[Email],[Salary],[Department])
Values (107,'MUKESH','Male','mkbb@gail.com',85000,'ADMIN')

SELECT * FROM EmployeeInfo;
-- Group by--
SELECT Department,SUM(Salary)FROM EmployeeInfo Group by Department
--Group by and having clause--
SELECT Department, SUM(Salary)FROM EmployeeInfo Group by Department having Department='ADMIN'
--
DECLARE @BigintValue AS bigint
SET @BigintValue =546787667877876565
SELECT @BigintValue AS BigintValueBeforeConversion,
--CONVERT(float,@BigintValue) AS ConvertedBigintValueToFloatValue,

CAST(@BigintValue as int) AS ConvertedBigintValueToIntValue
CAST(@BigintValue as tinyint) AS ConvertedBigintValueToTinyIntValue

--