using System;
using System.Collections.Generic;
using System.Text;

namespace Task1_25_11_20_Vehicle
{
    class Vehicle
    {
        private string name;
        private string color;
        int noOfWheels;
        int maxSpeed=70;
        public Vehicle()
        {
            name = "Tata";
            //Vehicle.name = "Tata";
            //Vehicle.color = "Green";
        }
        public void Start()
        {
            Console.WriteLine(name+"car is started and color is "+color);
        }
        public void Stop()
        {
            Console.WriteLine("car is stoped");
        }
        public void SpeedUp(int speedUp)
        {
            maxSpeed = speedUp;
        }
        public void Display()
        {
            Start();
            SpeedUp(80);
            Stop();

        }
    }
}