using System;

namespace MethodExtensioneExample01_12_20
{
    // Define an extension method in a non-nested static class.
    public static class Extensions
    {
        public static Grades minPassing = Grades.D;
        public static bool Passing(this Grades grade)
        {
            return grade >= minPassing;
        }
       
    }
    public static class IntExtensions
    {
        public static bool IsGreaterThan(this int i, int value)
        {
            return i > value;
        }
        public static bool IsLessThan(this int j, int value1)
        {
            return j < value1;
        }

    }
    public enum Grades { F = 0, D = 1, C = 2, B = 3, A = 4 };

    class Program
    {
        
        static void Main(string[] args)
        {
            Console.WriteLine("My method Extension program");

            Grades g1 = Grades.D;
            Grades g2 = Grades.F;
            Console.WriteLine("First {0} a passing grade.", g1.Passing() ? "is" : "is not");
            Console.WriteLine("Second {0} a passing grade.", g2.Passing() ? "is" : "is not");

            Extensions.minPassing = Grades.C;
           
            Console.WriteLine("First {0} a passing grade.", g1.Passing() ? "is" : "is not");
            Console.WriteLine("Second {0} a passing grade.", g2.Passing() ? "is" : "is not");

            Console.WriteLine("Second example for method extension");
            int i = 10;

            bool result = i.IsGreaterThan(100);

            Console.WriteLine("Your Result is "+result);
            Console.WriteLine("Third example for method extension");
            int j = 20;

            bool result1 = j.IsLessThan(50);

            Console.WriteLine("Your Result is " + result1);

        }
    }
}
