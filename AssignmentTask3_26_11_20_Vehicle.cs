using System;
using System.Collections.Generic;
using System.Text;

namespace AssignmentTask3_26_11_20_Vehicle
{
    public class Vehicle
    {
        //private string name;
        public string color;
        public int noOfWheels;
        protected  readonly int maxSpeed ;
        public Vehicle(string color1, int noOfwheels1, int maxSpeed1)
        {
            //name = "tata";
            color =color1 ;
            noOfWheels = noOfwheels1;
           maxSpeed = maxSpeed1;



        }
        //public void Start()
        //{
        //    Console.WriteLine(name+"car is started and color is "+color);
        //}
        //public void Stop()
        //{
        //    Console.WriteLine("car is stoped");
        //}
        //public void SpeedUp(int speedUp)
        //{
        //    maxSpeed = speedUp;
        //}
        //public void Display()
        //{
        //    Start();
        //    SpeedUp(80);
        //    Stop();

        //}
    }

    sealed class Car : Vehicle
    {
        string carName = "hyundai";
        public Car(string color, int noOfwheels, int maxSpeed) : base(color,noOfwheels,maxSpeed)
        {

        }

        public int CalculateTollamount()
        {
            int toll;
            toll = 60 * base.noOfWheels;
            // Console.WriteLine("bike toll is = "+toll);
            return toll;
        }
        public void Display()
        {
            Console.WriteLine(carName + base.color + base.noOfWheels + base.maxSpeed);
            Console.WriteLine("Toll amount to be paid is Rs. " + CalculateTollamount() + " only");
        }

    }
    sealed class Bike : Vehicle
    {
        string bikeName="Honda";
       public Bike (string a,int b,int c):base (a,b,c)
        {
            
        }
        public  int CalculateTollAmount()
        {
            int toll;
            toll = 2 * base.noOfWheels;
            // Console.WriteLine("bike toll is = "+toll);
            return toll;
        }
        public void Display ()
        {
            Console.WriteLine("your bike "+bikeName+" of color '"+ base.color +"' can run at max speed of " +base.maxSpeed);
            Console.WriteLine("Toll amount to be paid is Rs. "+CalculateTollAmount()+" only");
        }

    }




}
